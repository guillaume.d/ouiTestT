//
//  Utility.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 01/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import Foundation

class Utility {
    
    class func currrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMMM yyyy"
        formatter.locale = Locale(identifier: "FR-fr")
        return formatter.string(from: date)
    }
    
    
    class func temperatureString(temp: Float) -> String {
        
        let tempInt = Int(temp)
        return "\(tempInt)°"
    }
    
    class func getDescriptionIcon(description: String) -> String {
        var iconName = ""
        
        switch description {
        case "clear sky":
            iconName = "sun"
        case "broken clouds":
            iconName = "cloudSun"
        case "light rain":
            iconName = "rain"
        case "moderate rain":
            iconName = "rain"
        case "few clouds":
            iconName = "cloud"
        default:
            iconName = "cloudSun"
        }
        return iconName
    }
    
}
