//
//  MyButton.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 02/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import UIKit

@IBDesignable
class MyButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var borderWith: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.clear
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWith
        self.layer.borderColor = borderColor.cgColor
    }
}
