//
//  DayTableViewCell.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 02/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {

    //MARK: - Properties
    var meteoHour: HourModel? {
        didSet {
            updateUI()
        }
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var meteoDescription: UILabel!
    
    
    func updateUI() {
        self.backgroundColor = UIColor.FlatColor.Gray.WhiteSmoke
        dayLabel.text = meteoHour?.date
        meteoDescription.text = meteoHour?.weatherDescription
        iconView.image = UIImage(named: Utility.getDescriptionIcon(description: (meteoHour?.weatherDescription)!))
        if let noonTemp = meteoHour?.temperature {
            temperatureLabel.text = Utility.temperatureString(temp: noonTemp)
        }
        
    }
}
