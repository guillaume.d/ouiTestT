//
//  CityModel.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 01/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import Foundation

import Foundation

class CityModel {
    
    //Mark: - Properties
    
    let cityName: String
    var wheather: [DayWeather]
    
    
    init(name: String, wheatherArray:[DayWeather]) {
        cityName = name
        wheather = wheatherArray
    }
    
    
    
}
