//
//  HourModel.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 01/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import Foundation

class HourModel {
    
    //MARK: - Properties
        let icon: String
        let temperature: Float
        let weatherDescription: String
        let date: String
    
    
        init(iconW: String, dateW: String, temperatureW: Float, description: String) {
    
            icon = iconW
            date = dateW
            temperature = temperatureW
            weatherDescription = description
        }
    
    
    
}
