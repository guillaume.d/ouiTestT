//
//  DayWeather.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 01/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import Foundation

class DayWeather {
    
    //Mark: - Properties
    let dateOfTheDay: String
    let hourWeatherArray: [HourModel]
    
    init( dateOTD: String, hourArrat: [HourModel]) {
        dateOfTheDay = dateOTD
        hourWeatherArray = hourArrat
    }
    
}
