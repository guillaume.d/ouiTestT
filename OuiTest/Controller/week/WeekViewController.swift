//
//  WeekViewController.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 02/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import UIKit

class WeekViewController: UIViewController {
    
    //MARK: - Properties
    var dayArray: [DayWeather]?
    var hours: [HourModel]?
    //MARK: - IBOutlets
    @IBOutlet weak var weekTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Semaine"
        hours = dayArray?.first?.hourWeatherArray
        
        weekTableView.delegate = self
        weekTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WeekViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (hours?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath) as? DayTableViewCell

        
        if let days = dayArray {
            let day = days.first

            cell?.meteoHour = day?.hourWeatherArray[indexPath.row]
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

    }
    
}
