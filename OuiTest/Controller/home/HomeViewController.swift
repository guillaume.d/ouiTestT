
//
//  HomeViewController.swift
//  OuiTest
//
//  Created by Guillaume Dadouche on 01/12/2017.
//  Copyright © 2017 Guillaume Dadouche. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK: - Properties
    var cityModel: CityModel?
    var weatherArray = [DayWeather]()
    var weatherByHour = [HourModel]()
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var ColorView: UIView!
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Oui test"
        loadData(completion: {self.updateUI()})
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI() {
        print("after load !!!")
        
        DispatchQueue.main.async {
            self.ColorView.backgroundColor = UIColor.FlatColor.Red.TerraCotta
            self.townLabel.text = self.cityModel?.cityName
            
            let day = self.cityModel?.wheather.first
            let currentHourWeather = day?.hourWeatherArray.first
            if let floatTemp = currentHourWeather?.temperature {
                 self.temperatureLabel.text = Utility.temperatureString(temp: floatTemp)
                
            }
            if let weatherDescription = currentHourWeather?.weatherDescription {
                let iconName = Utility.getDescriptionIcon(description: weatherDescription)
                self.iconView.image = UIImage(named: iconName)
            }
            
        }
    }
    
    private func loadData(completion: @escaping()-> () ) {
        //let ParisID = 2988507
        let parisStringUrl = "http://openweathermap.org/data/2.5/forecast?id=2988507&appid=b1b15e88fa797225412429c1c50c122a1"
        let urlForParis = URL(string:parisStringUrl)
        
        print("URL session ....")
        URLSession.shared.dataTask(with: urlForParis!) { (data, response, err ) in
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:Any]
                
                let city = jsonResult["city"] as! [String:Any]
                print(city["name"]!)
                guard let cityName = city["name"] as? String else {return}
                
                let list = jsonResult["list"]
                
                for val in list as! NSArray  {
                    let WObject = val as! [String:Any]
                    
                    let dateString = WObject["dt_txt"] as? String
                    let weatherArray = WObject["weather"] as! NSArray
                    
                    // icon
                    var icon: String?
                    var description: String?
                    
                    for values in weatherArray {
                        
                        let wDictionary = values as! [String:Any]
                         icon = wDictionary["icon"] as? String
                        description = wDictionary["description"] as? String
                    }
                    
                    // temperature
                    let main = WObject["main"] as! [String:Any]
                    let temperature = main["temp"] as? Float
                    
                    // create hourModel
                    let hourModel = HourModel(iconW: icon!, dateW: dateString!, temperatureW: temperature!, description: description!)
                    self.weatherByHour.append(hourModel)
                }

                //create DayWeather
                let dayWeather = DayWeather(dateOTD: Utility.currrentDate(), hourArrat: self.weatherByHour)
                self.weatherArray.append(dayWeather)
                // create city
                self.cityModel = CityModel(name: cityName , wheatherArray: self.weatherArray)
                
            }catch let jsonErr {
                print("error serializing json :", jsonErr)
            }
            completion()
        }.resume()
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch identifier {
            case "segueToWeek":
                let destination = segue.destination as! WeekViewController
                destination.dayArray = cityModel?.wheather
            default: break
            }
        }
    }

}

